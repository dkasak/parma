.PHONY: default install build build-release

default: build

build:
	cargo build
	

build-release:
	cargo build --release

install: build-release
	cargo install --path .
	mkdir -p ~/.config/zsh/completions/
	mkdir -p ~/.config/parma
	mkdir -p ~/.local/share/parma/style
	mkdir -p ~/.local/share/parma/templates
	mkdir -p ~/.local/share/parma/contrib
	install ./example/parma-menu.conf ~/.config/parma
	install ./style/* ~/.local/share/parma/style
	install ./templates/* ~/.local/share/parma/templates
	install ./contrib/* ~/.local/share/parma/contrib
	ls -t ./target/debug/build/parma-*/out/_parma | head -n 1 | xargs cp -t ~/.config/zsh/completions/
