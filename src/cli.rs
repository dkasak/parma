use clap::{App, AppSettings, Arg, SubCommand};

pub fn build_cli() -> App<'static, 'static> {
    App::new("parma")
        .version("0.2")
        .about("Note-taking tool")
        .author("Denis Kasak <dkasak@termina.org.uk>")
        .global_setting(AppSettings::ColorAuto)
        .global_setting(AppSettings::ColoredHelp)
        .setting(AppSettings::DeriveDisplayOrder)
        .max_term_width(100)
        .subcommand(
            SubCommand::with_name("new")
                .about("Create a new note.")
                .long_about(
                    "\nCreate a new note.\n\n\
                     Spawns an editor to let you edit the note. Content piped in or passed via the\n\
                     -f flag will be used to populate the `initial_text` template variable.\n",
                )
                .arg(
                    Arg::with_name("initial_text")
                        .takes_value(true)
                        .value_name("TEXT")
                        .required(false)
                        .help("Initial note content.")
                        .conflicts_with("file")
                )
                .arg(
                    Arg::with_name("file")
                        .short("-f")
                        .long("--file")
                        .takes_value(true)
                        .value_name("FILE")
                        .required(false)
                        .help("Take the note text from FILE.")
                        .conflicts_with("template"),
                )
                .arg(
                    Arg::with_name("title")
                        .long("--title")
                        .takes_value(true)
                        .value_name("TITLE")
                        .required(false)
                        .help("Set note title to TITLE")
                        .long_help("\nSet note title to TITLE.\n\n\
                                    Note: The value TITLE will only be used if the template references the `title` variable.\n\
                        "),
                )
                .arg(
                    Arg::with_name("template")
                        .short("-t")
                        .long("--template")
                        .takes_value(true)
                        .value_name("TEMPLATE")
                        .required(false)
                        .help("Initialize note from template TEMPLATE.")
                        .long_help("\nInitialize note from template TEMPLATE.\n\n\
                                    The following template variables will be replaced with a value:\n\n\
                                    - `date`: the current date\n\
                                    - `author`: the author of the note (as set in the config file)\n\
                                    - `title`: the title of the note\n\
                                    - `initial_text`: initial note text\n\n\
                                    Use the special template `none` to start with only the initial\n\
                                    text, without a note header.\n\
                        ").conflicts_with("file"),
                ),
        )
        .subcommand(
            SubCommand::with_name("import")
                .about("Import an existing note, either from file or stdin.")
                .arg(
                    Arg::with_name("file")
                        .short("-f")
                        .long("--file")
                        .takes_value(true)
                        .value_name("FILE")
                        .required(false)
                        .help("Import note from FILE")
                )
        )
        .subcommand(
            SubCommand::with_name("edit")
                .about("Edit a note (spawns an editor).")
                .arg(Arg::with_name("id")
                         .value_name("NOTE_ID")
                         .required(false)
                         .conflicts_with("last_note")
                         .help("ID of a note"))
                .arg(
                    Arg::with_name("last_note")
                        .long("--last")
                        .takes_value(false)
                        .required(false)
                        .help("Edit the note that was last created or edited.")
                )
                .arg(
                    Arg::with_name("append")
                        .short("-a")
                        .long("--append")
                        .value_name("TEXT")
                        .takes_value(true)
                        .required(false)
                        .help("Append the supplied text to the note.")
                )
        )
        .subcommand(
            SubCommand::with_name("show")
                .arg(Arg::with_name("id")
                         .value_name("NOTE_ID")
                         .required(false)
                         .help("ID of a note"))
                .arg(
                    Arg::with_name("json")
                        .short("j")
                        .long("json")
                        .help("Output the note in JSON format."),
                )
                .about("Show a note."),
        )
        .subcommand(
            SubCommand::with_name("delete")
                .about("Delete a note.")
                .visible_alias("rm")
                .arg(Arg::with_name("id")
                         .value_name("NOTE_ID")
                         .required(true)
                         .help("ID of a note"))
        )
        .subcommand(
            SubCommand::with_name("render")
                .about("Render a note into an output format.")
                .arg(Arg::with_name("id")
                         .value_name("NOTE_ID")
                         .required(false)
                         .help("ID of a note"))
                .arg(
                    Arg::with_name("format")
                        .short("-f")
                        .long("--format")
                        .default_value("html")
                        .possible_values(&["html", "pdf"])
                        .help("Format to render the note into"),
                )
                .arg(
                    Arg::with_name("file")
                        .short("-o")
                        .long("--output-file")
                        .takes_value(true)
                        .help("File to save the rendered note into"),
                )
                .arg(
                    Arg::with_name("standalone").long("--standalone").help(
                        "Produce a standalone document. Only has an effect on HTML rendering.",
                    ),
                )
                .arg(
                    Arg::with_name("resource_location")
                        .long("--resource-location")
                        .takes_value(true)
                        .help("URL prefix used when resolving templates, stylesheets, etc."),
                )
        )
        .subcommand(
            SubCommand::with_name("ids")
                .about("List all note IDs.")
                .visible_alias("list")
                .visible_alias("ls")
        )
        .subcommand(
            SubCommand::with_name("from-legacy")
                .about(
                    "Convert a legacy note storage into the new JSON storage and output to stdout.",
                )
                .arg(
                    Arg::with_name("path")
                        .required(true)
                        .help("Path to notes file in the legacy format."),
                )
        )
        .subcommand(
            SubCommand::with_name("rofi").about("Produce output suitable for piping into rofi."),
        )
        .subcommand(SubCommand::with_name("sync").about("WIP: Sync notes to Matrix."))
        .subcommand(
            SubCommand::with_name("annotate")
                .about("WIP: Annotate a file, optionally specifying a line and column.")
                .arg(Arg::with_name("file")
                         .required(true)
                         .help("File to annotate"))
                .arg(
                    Arg::with_name("line")
                        .takes_value(true)
                        .required(false)
                        .help("Line to annotate")
                )
                .arg(
                    Arg::with_name("column")
                        .takes_value(true)
                        .required(false)
                        .requires("line")
                        .help("Column to annotate")
                )
        )
        .subcommand(
            SubCommand::with_name("tag")
                .about("Tag note(s).")
                .arg(Arg::with_name("id")
                         .value_name("NOTE_ID")
                         .required(false)
                         .help("ID of a note"))
                .arg(Arg::with_name("add_tag")
                         .long("--add")
                         .required(false)
                         .takes_value(true)
                         .help("Tag to add"))
                .arg(Arg::with_name("remove_tag")
                         .long("--remove")
                         .takes_value(true)
                         .help("Tag to remove"))
        )
        .subcommand(
            SubCommand::with_name("search")
                .about("Search through your notes.")
                .arg(Arg::with_name("title")
                         .long("--title")
                         .required(false)
                         .help("Search note titles"))
                .arg(Arg::with_name("text")
                         .long("--text")
                         .required(false)
                         .help("Search note text"))
                .arg(Arg::with_name("topic")
                         .long("--topic")
                         .required(false)
                         .help("Search note topic"))
                .arg(Arg::with_name("keywords")
                         .long("--keywords")
                         .required(false)
                         .help("Search note keywords"))
                .arg(Arg::with_name("any")
                         .long("--any")
                         .required(false)
                         .help("Search all of the above fields"))
                .arg(Arg::with_name("id")
                         .short("-i")
                         .long("--id")
                         .required(false)
                         .help("Output just matched note IDs"))
                .arg(Arg::with_name("json")
                         .short("-j")
                         .long("--json")
                         .required(false).help("Output matched notes as JSON"))
                .arg(Arg::with_name("regex")
                        .value_name("REGEX")
                        .takes_value(true)
                        .required(true)
                        .help("Regex to search for"))
            )
        .subcommand(
            SubCommand::with_name("menu")
                .about("Spawns a rofi-based menu for interacting with your notes.")
            )
}
