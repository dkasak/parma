use std::env::var;
use std::fs;
use std::fs::File;
use std::io::Write;
use std::path::PathBuf;

use serde::{Deserialize, Serialize};

use crate::error;

#[derive(Serialize, Deserialize)]
pub struct Config {
    /// Your name, used for attribution in the notes you create.
    pub author: Option<String>,

    /// Path to notes file.
    pub notes_path: PathBuf,
    pub editor: Option<Editor>,
    pub terminal: Option<String>,

    // XXX: separate these into a separate RuntimeConfig struct?
    pub interactive_mode: Option<bool>,
    pub resource_location: Option<String>,

    #[cfg(feature = "matrix")]
    pub matrix: Option<Matrix>,
}

#[derive(Serialize, Deserialize, Clone)]
pub struct Editor {
    pub command: String,
    pub in_terminal: bool,
}

#[cfg(feature = "matrix")]
#[derive(Serialize, Deserialize, Clone)]
pub struct Matrix {
    /// Matrix ID (MXID).
    pub username: String,

    /// Matrix homeserver URL
    pub homeserver_url: String,

    /// Matrix room ID for the notes room.
    pub room: String,
}

impl Config {
    /// Return the default configuration.
    ///
    /// Side-effect: if the data directory does not exist, it is created.
    pub fn default() -> Config {
        let dat_dir = Self::data_dir();

        Config {
            author: None,
            notes_path: dat_dir.join("notes.json"),
            editor: None,
            terminal: None,
            interactive_mode: None,
            resource_location: None,

            #[cfg(feature = "matrix")]
            matrix: None,
        }
    }

    /// Calculate path of the data directory.
    ///
    /// Side-effect: if the directory does not exist, it is created.
    pub fn data_dir() -> PathBuf {
        let mut dat_dir = dirs::data_dir()
            .unwrap_or_else(|| error!("Cannot determine data directory on your platform.", 1));

        dat_dir.push("parma");

        if !dat_dir.exists() {
            fs::create_dir_all(&dat_dir).unwrap_or_else(|_| error!("Cannot create data dir.", 1))
        }

        dat_dir
    }

    /// Calculate path of the config directory.
    ///
    /// Side-effect: if the directory does not exist, it is created.
    pub fn config_dir() -> PathBuf {
        let mut cfg_dir = dirs::config_dir()
            .unwrap_or_else(|| error!("Cannot determine config directory on your platform.", 1));

        cfg_dir.push("parma");

        if !cfg_dir.exists() {
            fs::create_dir_all(&cfg_dir).unwrap_or_else(|_| error!("Cannot create config dir.", 1))
        }

        cfg_dir
    }

    /// Calculate path of the cache directory.
    ///
    /// Side-effect: if the directory does not exist, it is created.
    pub fn cache_dir() -> PathBuf {
        let mut cache_dir = dirs::cache_dir()
            .unwrap_or_else(|| error!("Cannot determine cache directory on your platform.", 1));

        cache_dir.push("parma");

        if !cache_dir.exists() {
            fs::create_dir_all(&cache_dir).unwrap_or_else(|_| error!("Cannot create cache dir.", 1))
        }

        cache_dir
    }

    /// Calculate path of the config file.
    ///
    /// Side-effect: if the file or any of its parent directories do not exist, they are created.
    pub fn config_path() -> PathBuf {
        Self::config_dir().join("config.toml")
    }

    /// Load the config from the default config file.
    ///
    /// Side-effect: if the config file does not exist, it is created and pre-filled with the
    /// default configuration.
    pub fn load() -> Config {
        let cfg_path = Self::config_path();
        let config = match fs::read(&cfg_path) {
            Err(_) => {
                let default_config = Self::default();
                let mut cfg_file = File::create(cfg_path)
                    .unwrap_or_else(|_| error!("Cannot create config file.", 1));
                let default_config_str = toml::to_string_pretty(&default_config).unwrap();
                cfg_file.write_all(default_config_str.as_bytes()).unwrap();

                default_config
            }
            Ok(v) => {
                let s = String::from_utf8_lossy(&v);
                toml::from_str(&s).unwrap_or_else(|e| error!(e, 1))
            }
        };

        config
    }

    /// Try to determine the text editor to use.
    ///
    /// On success, the returned `Editor` object contains the editor's command and whether it
    /// should be run in a terminal.
    pub fn editor(&self) -> Option<Editor> {
        self.editor.clone().or_else(|| {
            let editor_cmd = var("EDITOR").ok()?;

            let in_terminal = match editor_cmd.as_ref() {
                "vim" | "nvim" | "pico" | "nano" | "emacs" => true,
                "gedit" => false,
                _ => true,
            };

            Some(Editor {
                command: editor_cmd,
                in_terminal,
            })
        })
    }
}
