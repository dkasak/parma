#![feature(try_blocks)]

pub mod cli;
pub mod config;
pub mod note;
pub mod storage;
#[macro_use]
pub mod util;
pub mod parma;
pub mod render;
pub mod rofi;
pub mod templates;

use std::borrow::Cow;
use std::io::{stdin, stdout, Read, Write};
use std::path::PathBuf;
use std::process::Command;

use anyhow::{anyhow, Context, Result};
use atty::Stream;
use pandoc::{PandocError, PandocOutput};
use serde::Serialize;
use tinytemplate::TinyTemplate;
use tracing::{info, trace};

use crate::config::Config;
use crate::note::Note;
use crate::parma::Parma;
use crate::rofi::note_to_rofi;
use crate::storage::legacy::Legacy;

/// Print IDs of all notes to stdout.
fn parma_ids(config: &Config) -> Result<()> {
    let parma = Parma::from_file(&config.notes_path)?;

    for note in parma {
        println!("{}", note.id);
    }

    Ok(())
}

#[derive(Serialize)]
struct DefaultTemplateContext {
    author: String,
    initial_text: String,
    date: String,
    title: Option<String>,
}

#[derive(Serialize)]
struct AnnotationTemplateContext {
    author: String,
    initial_text: String,
    date: String,
    annotation_for: PathBuf,
    annotation_line: Option<u32>,
    annotation_column: Option<u32>,
}

/// Create a new note.
///
/// Spawns an editor to let you edit the note. Content piped in or passed via the -f flag will be
/// used to populate the `initial_text` template variable.
fn parma_new(args: &clap::ArgMatches, config: &Config) -> Result<()> {
    let cmd_content = args.value_of("initial_text").map(|s| s.to_string());
    let file_arg = args.value_of("file");
    let template_name = args.value_of("template");
    let title = args.value_of("title");

    let mut template = TinyTemplate::new();
    let template_contents = match template_name {
        // the "empty" template, containing only the initial_text but no header
        Some("none") => "{initial_text}".to_owned(),

        Some(template_name) => std::fs::read_to_string(
            Config::data_dir()
                .join("templates")
                .join(template_name)
                .to_str()
                .unwrap(),
        )
        .context("No such template")?,

        None => templates::DEFAULT_TEMPLATE.to_string(),
    };

    template
        .add_template("note", &template_contents)
        .context("Failed instantiating template")?;

    let initial_text = if config.interactive_mode.unwrap() {
        cmd_content.or_else(|| {
            file_arg.and_then(|note_path| {
                let note_path = PathBuf::from(note_path);
                let note_content = std::fs::read_to_string(&note_path)
                    .with_context(|| {
                        format!(
                            "Failed reading note from file {}",
                            &note_path.as_path().display()
                        )
                    })
                    .ok()?;

                Some(note_content)
            })
        })
    } else {
        let mut note_content = String::new();
        stdin().read_to_string(&mut note_content)?;

        if note_content.trim() == "" {
            None
        } else {
            Some(note_content.trim_end().to_string())
        }
    };

    let has_initial_text = initial_text.is_some();

    trace!("has_initial_text = {:#?}", has_initial_text);
    trace!("initial_text = {:#?}", initial_text);

    let context = DefaultTemplateContext {
        initial_text: initial_text.unwrap_or(String::new()),
        /// TODO: Use system username as the default if author is unset?
        author: config.author.clone().unwrap_or_default(),
        date: util::date::today_as_string(),
        title: title.map(|s| s.to_owned()),
    };

    let note_file = util::temp_markdown_file()?;
    let note_path = note_file.path().to_owned();

    let mut note_content = template
        .render("note", &context)
        .context("Failed rendering template")?;

    std::fs::write(&note_path, &note_content).context("Failed writing initial content to note")?;

    let initial_note_content = note_content.clone();

    let note = loop {
        util::editor::edit(config, &note_path)?;
        note_content =
            std::fs::read_to_string(&note_path).context("Failed reading back your input")?;
        note_content = util::remove_leading_html_comments(&note_content);

        // If the note is empty once the feedback HTML comment is stripped, assume the
        // user just wants to quit.
        if note_content.trim().len() == 0 {
            return Ok(());
        }

        std::fs::write(&note_path, &note_content)
            .context("Failed opening note file while erasing feedback.")?;

        match Note::from_markdown(Cow::Owned(note_content.clone())) {
            Ok(note) => break note,
            Err(e) => {
                util::editor::prepend(
                    format!(
                        "<!-- Invalid note, please edit and try again.\n\n{}\n-->\n\n",
                        e
                    )
                    .as_ref(),
                    note_path.as_path(),
                )
                .context("Failed opening note file while writing feedback.")?;
            }
        }
    };

    let note_id = note.id.clone().to_string();

    trace!(
        "initial_note_content.trim() = {:#?}",
        initial_note_content.trim()
    );
    trace!("note_content.trim() = {:#?}", note_content.trim());

    if has_initial_text
        || (initial_note_content.trim() != note_content.trim() && note_content.trim() != "")
    {
        info!("Storing new note with id {}", note_id);

        let mut parma = Parma::from_file(&config.notes_path)?;
        parma.insert(note)?;
        parma
            .store(&config.notes_path)
            .context("Failed storing new note")?;

        // Write the ID of the note we touched last. We don't care much if the write fails.
        let _ = std::fs::write(Config::cache_dir().join("last_note"), &note_id);
    }

    Ok(())
}

/// Import an existing note, either from file or stdin.
fn parma_import(args: &clap::ArgMatches, config: &Config) -> Result<()> {
    let file_arg = args.value_of("file");

    let piped = !config.interactive_mode.unwrap();
    if !piped && file_arg.is_none() {
        return Err(anyhow!("Missing note. Either pipe the note's contents or pass a file containing the note using -f."));
    }

    let note = if piped {
        // Note is being piped into parma.

        let mut note_content = String::new();
        stdin().read_to_string(&mut note_content)?;

        match Note::from_markdown(Cow::Owned(note_content)) {
            Ok(note) => note,

            Err(e) => {
                return Err(anyhow!(e));
            }
        }
    } else {
        // Note is being imported from a file.

        let note_path = PathBuf::from(file_arg.unwrap());
        let note_content = std::fs::read_to_string(&note_path).with_context(|| {
            format!(
                "Failed reading note from file {}",
                &note_path.as_path().display()
            )
        })?;

        match Note::from_markdown(Cow::Owned(note_content)) {
            Ok(note) => note,

            Err(e) => {
                return Err(anyhow!(e));
            }
        }
    };

    let mut parma = Parma::from_file(&config.notes_path)?;
    parma.insert(note)?;
    parma
        .store(&config.notes_path)
        .context("Failed storing imported note")?;

    Ok(())
}

/// Edit a note (spawns an editor).
fn parma_edit(args: &clap::ArgMatches, config: &Config) -> Result<()> {
    let note_id = if args.is_present("last_note") {
        std::fs::read_to_string(Config::cache_dir().join("last_note")).context("No last note.")?
    } else {
        match args.value_of("id") {
            Some(id) => id.to_string(),
            _ => {
                return Err(anyhow!("Missing note ID."));
            }
        }
        .to_string()
    };

    let note_content;
    {
        let mut parma = Parma::from_file(&config.notes_path)?;
        let note = parma
            .note_with_id(&note_id)
            .with_context(|| format!("No note with ID: {}", note_id))?;

        note_content = format!("{}", note);
    }

    let note_file = util::temp_markdown_file()?;
    let note_path = note_file.path().to_owned();

    std::fs::write(&note_path, note_content).context("Failed instantiating note")?;

    let append_text = args.value_of("append");
    let edited_note = match append_text {
        Some(text) => {
            let old_content =
                std::fs::read_to_string(&note_path).context("Failed reading back your input")?;
            let new_content = format!("{}\n{}", old_content, text.to_string());
            Note::from_markdown_with_id(Cow::Borrowed(&note_id), Cow::Owned(new_content)).context(
                "The text that was appended broke the note structure. Probably shouldn't happen.",
            )?
        }

        None => loop {
            util::editor::edit(config, &note_path)?;
            let markdown_input =
                std::fs::read_to_string(&note_path).context("Failed reading back your input")?;
            let markdown_input = util::remove_leading_html_comments(&markdown_input);

            match Note::from_markdown_with_id(Cow::Borrowed(&note_id), Cow::Owned(markdown_input)) {
                Ok(note) => break note,
                Err(e) => {
                    let markdown_input = std::fs::read_to_string(&note_path)
                        .context("Failed reading back your input")?;
                    let markdown_input = util::remove_leading_html_comments(&markdown_input);
                    let input_with_feedback = &format!(
                        "<!-- Invalid note, please edit and try again.\n\n{}\n-->\n\n{}",
                        e, markdown_input
                    );

                    std::fs::write(&note_path, input_with_feedback)
                        .context("Failed writing back note with feedback")?;
                }
            }
        },
    };

    // Replace original note with the edited note.
    // FIXME: We try to avoid race condition by only opening the database for a short amount of
    // time when reading/writing, but to fully and properly handle, we'll need to either separate
    // the notes into one note per file or use a proper database (sqlite).
    let mut parma = Parma::from_file(&config.notes_path)?;
    let note = parma
        .note_with_id(&note_id)
        .with_context(|| format!("No note with ID: {}", note_id))?;
    *note = edited_note;

    parma
        .store(&config.notes_path)
        .context("Failed storing edited note")?;

    // Write the ID of the note we touched last. We don't care much if the write fails.
    let _ = std::fs::write(Config::cache_dir().join("last_note"), &note_id);

    Ok(())
}

/// Show a note's source (as Markdown).
fn parma_show(args: &clap::ArgMatches, config: &Config) -> Result<()> {
    let mut ids: Vec<&str>;
    let mut stdin_input = String::new();

    // Get note IDs from stdin.
    if !config.interactive_mode.unwrap() {
        stdin()
            .read_to_string(&mut stdin_input)
            .context("Failed to read note IDs from stdin")?;
    }
    ids = stdin_input.lines().collect();

    // Append command-line note ID, if it was supplied.
    args.value_of("id").map(|x| ids.push(x));

    // Show the notes.
    let mut parma = Parma::from_file(&config.notes_path)?;
    for id in ids {
        match parma.note_with_id(id) {
            Some(note) => {
                if args.is_present("json") {
                    println!("{}", serde_json::to_string_pretty(note).unwrap());
                } else {
                    println!("{}\n{}", note.header, note.text);
                }
            }
            None => {
                eprintln!("No note with ID: {}", id);
            }
        };
    }

    Ok(())
}

/// Tag note(s). Add or remove existing tag(s).
fn parma_tag(args: &clap::ArgMatches, config: &Config) -> Result<()> {
    let mut ids: Vec<&str>;
    let mut stdin_input = String::new();

    // If stdin is piped, get note IDs from stdin.
    if !config.interactive_mode.unwrap() {
        stdin()
            .read_to_string(&mut stdin_input)
            .context("Failed to read note IDs from stdin")?;
    }
    ids = stdin_input.lines().collect();

    // Append command-line note ID, if it was supplied.
    args.value_of("id").map(|x| ids.push(x));

    let tag_to_add = args.value_of("add_tag");
    let tag_to_remove = args.value_of("remove_tag");

    // Tag notes
    let mut parma = Parma::from_file(&config.notes_path)?;
    let mut changed = 0;

    for id in ids {
        match parma.note_with_id(id) {
            Some(note) => {
                if let Some(tag) = tag_to_add {
                    let result = note.add_tag(tag.into());

                    if result.is_some() {
                        changed += 1;
                    }

                    result
                } else {
                    None
                };

                if let Some(tag) = tag_to_remove {
                    let result = note.remove_tag(tag.into());

                    if result.is_some() {
                        changed += 1;
                    }

                    result
                } else {
                    None
                };
            }
            None => {
                eprintln!("No note with ID: {}", id);
            }
        };
    }

    println!("Changed {} notes.", changed);

    // Store result
    parma
        .store(&config.notes_path)
        .context("Failure while trying to store notes")?;

    Ok(())
}

/// Delete a note.
fn parma_delete(args: &clap::ArgMatches, config: &Config) -> Result<()> {
    let note_id = args.value_of("id").unwrap();
    let mut parma = Parma::from_file(&config.notes_path)?;

    parma
        .delete(note_id)
        .with_context(|| format!("No note with ID: {}", note_id))?;

    parma
        .store(&config.notes_path)
        .context("Failure while trying to store notes")?;

    println!("Note {} deleted.", note_id);

    Ok(())
}

/// Render a note into an output format.
fn parma_render(args: &clap::ArgMatches, config: &Config) -> Result<()> {
    let note = if let Some(note_id) = args.value_of("id") {
        let mut parma = Parma::from_file(&config.notes_path)?;
        parma
            .note_with_id(note_id)
            .with_context(|| format!("No note with ID: {}", note_id))?
            .to_owned()
    } else {
        let mut stdin_input = String::new();
        stdin()
            .read_to_string(&mut stdin_input)
            .context("Cannot read stdin.")?;
        Note::from_markdown(stdin_input.into())
            .context("stdin input does not represent a valid note.")?
    };

    let format = args.value_of("format").unwrap();
    let result: Result<PandocOutput, PandocError> = match format {
        "html" => render::to_html(args, config, &note),
        "pdf" => render::to_pdf(config, &note),
        _ => return Err(anyhow!(format!("Unsupported render format: {}", format))),
    };

    match result.context("Error rendering note")? {
        PandocOutput::ToBufferRaw(output) => {
            let out_file = args.value_of("file");

            match out_file {
                Some(path) => std::fs::write(path, output).context("Failed writing to file")?,
                None => std::io::stdout()
                    .write_all(&output)
                    .context("Failed writing to stdout")?,
            }
        }
        PandocOutput::ToBuffer(output) => {
            let out_file = args.value_of("file");

            match out_file {
                Some(path) => std::fs::write(path, output).context("Failed writing to file")?,
                None => println!("{}", output),
            }
        }
        PandocOutput::ToFile(_) => {
            panic!("pandoc bug: we requested output to a pipe, but it wrote to a file")
        }
    }

    Ok(())
}

/// Convert a legacy note file into a JSON file (printed on stdout).
fn parma_from_legacy(args: &clap::ArgMatches) -> Result<()> {
    let mut notes: Vec<Note> = Vec::new();
    let mut legacy_parser = Legacy::new();

    let notes_path = args.value_of("path").unwrap();
    let storage_contents = std::fs::read_to_string(notes_path)
        .with_context(|| format!("Unable to open note file: {}", notes_path))?;

    let legacy_notes = legacy_parser.parse(&storage_contents)?;

    for (id, text) in legacy_notes {
        match Note::from_markdown_with_id(Cow::Owned(id), Cow::Owned(text)) {
            Ok(note) => notes.push(note),
            Err(e) => {
                eprintln!("{}", e)
            }

        // FIXME: Start producing some of these again and propagate them via thiserror.
        // Err(NoteParsingError::BadKeyword) => {
        //     eprintln!("ERROR: Bad keyword in note {}.", id)
        // }
        // Err(NoteParsingError::InvalidDate) => {
        //     eprintln!("ERROR: Invalid date in note {}", id)
        // }
        // Err(NoteParsingError::NoKeywords) => eprintln!("ERROR: No keywords in note {}", id),
        }
    }

    let json = serde_json::to_string_pretty(&notes).context("Failed serializing notes to JSON")?;
    println!("{}", json);

    Ok(())
}

/// Create a notes file suitable for notemenu/rofi and output to stdout.
fn parma_rofi(config: &Config) -> Result<()> {
    let parma = Parma::from_file(&config.notes_path)?;

    for line in parma.iter().map(note_to_rofi) {
        println!("{}", line);
    }

    Ok(())
}

/// Sync notes to Matrix.
#[cfg(feature = "matrix")]
fn parma_sync(config: &Config) -> Result<()> {
    use matrix_sdk::identifiers::RoomId;
    use matrix_sdk::{identifiers::UserId, Session};
    use std::convert::TryFrom;

    let runtime = tokio::runtime::Builder::new_current_thread().build()?;

    if let Some(matrix) = &config.matrix {
        let mut session_file_path = Config::data_dir();
        session_file_path.push("session.json");

        let homeserver_url = url::Url::parse(&matrix.homeserver_url)?;
        let client = matrix_sdk::Client::new(homeserver_url)?;

        if session_file_path.exists() {
            println!("Previous session found, restoring...");
            let session = std::fs::read_to_string(session_file_path)?;
            let session = serde_json::from_str(&session)?;
            runtime.block_on(client.restore_login(session))?;
        } else {
            println!("Logging in...");

            let password =
                rpassword::read_password_from_tty(Some("Enter Matrix account password: ")).unwrap();
            let response = runtime.block_on(client.login(
                &matrix.username,
                &password,
                None,
                Some(&"parma".to_owned()),
            ))?;

            println!("access_token: {}", response.access_token);

            let session = Session {
                access_token: response.access_token,
                user_id: UserId::try_from(matrix.username.as_ref()).unwrap(),
                device_id: response.device_id,
            };

            std::fs::write(session_file_path, serde_json::to_string(&session)?)?;
        }

        println!("Logged in!");

        let parma = Parma::from_file(&config.notes_path)?;

        let note = parma.iter().next();
        if let Some(note) = note {
            println!("{:#?}", note);

            let room_id = RoomId::try_from(matrix.room.as_ref())?;

            runtime
                .block_on(client.room_send(&room_id, &*note, None))
                .context("Failed sending note to Matrix.")?;
        }

    // println!(
    //     "{:#?}",
    //     runtime.block_on(client.sync(matrix_sdk::SyncSettings::new()))
    // );
    } else {
        println!("No Matrix section in config.")
    }

    Ok(())
}

/// Annotate a file, optionally specifying a line and column.
fn parma_annotate(args: &clap::ArgMatches, config: &Config) -> Result<()> {
    let file = args.value_of("file").unwrap();
    let line = args.value_of("line");
    let column = args.value_of("column");

    // let note = Note::from_json_value(note)

    let mut template = TinyTemplate::new();
    let template_contents = templates::ANNOTATION_TEMPLATE.to_string();
    // let template_contents = match None {
    //     Some(template_name) => std::fs::read_to_string(
    //     Some(..) => std::fs::read_to_string(
    //         Config::data_dir()
    //             .join("templates")
    //             .join(template_name)
    //             .to_str()
    //             .unwrap(),
    //     )
    //     .context("No such template")?,

    //     None => templates::ANNOTATION_TEMPLATE.to_string(),
    // };

    template
        .add_template("annotation", &template_contents)
        .context("Failed instantiating template")?;

    print!("{:#?}", line.unwrap().parse::<u32>());

    let context = AnnotationTemplateContext {
        initial_text: String::new(),
        /// TODO: Use system username as the default if author is unset?
        author: config.author.clone().unwrap_or_default(),
        date: util::date::today_as_string(),
        annotation_for: PathBuf::from(file).canonicalize().unwrap(),
        annotation_line: line.and_then(|l| l.parse::<u32>().ok()),
        annotation_column: column.and_then(|l| l.parse::<u32>().ok()),
    };

    println!("{:#?}", serde_json::to_value(&context).unwrap());

    let annotation_content = template
        .render("annotation", &context)
        .context("Failed rendering template")?;

    let annotation = Note::from_markdown(Cow::Owned(annotation_content))
        .context("Failed parsing annotation.")?;

    let mut parma = Parma::from_file(&config.notes_path)?;
    parma.insert(annotation)?;
    parma
        .store(&config.notes_path)
        .context("Failed storing imported note")?;

    Ok(())
}

fn parma_search(args: &clap::ArgMatches) -> Result<()> {
    let regex = args.value_of("regex").unwrap();
    let as_json = args.is_present("json");

    let search_what;
    if args.is_present("title") {
        search_what = "--title";
    } else if args.is_present("text") {
        search_what = "--text";
    } else if args.is_present("keywords") {
        search_what = "--keywords";
    } else {
        search_what = "--any";
    }

    let search_args;
    if as_json {
        search_args = vec![search_what, "--json", regex];
    } else {
        search_args = vec![search_what, "--ids-only", regex];
    }

    let search_cmd = Config::data_dir().join("contrib").join("parma-search");

    stdout()
        .write_all(
            &Command::new(search_cmd)
                .args(&search_args)
                .output()
                .context("Failed getting output from parma-search.")?
                .stdout,
        )
        .context("Failed writing parma-search output to stdout.")?;

    Ok(())
}

fn parma_menu(_args: &clap::ArgMatches) -> Result<()> {
    let menu_cmd = Config::data_dir().join("contrib").join("parma-menu");

    stdout()
        .write_all(
            &Command::new(menu_cmd)
                .output()
                .context("Failed getting output from parma-menu.")?
                .stdout,
        )
        .context("Failed writing parma-menu output to stdout.")?;

    Ok(())
}

fn main() -> Result<()> {
    tracing_subscriber::fmt::init();

    // Parse command-line arguments
    let args = cli::build_cli().get_matches();
    // .gen_completions("parma", Shell::Zsh, "compl");

    let mut config = Config::load();
    config.interactive_mode = Some(atty::is(Stream::Stdin));
    config.resource_location = Some(format!(
        "file://{}",
        Config::data_dir().join("style").to_str().unwrap()
    ));

    // Handle subcommands
    match args.subcommand() {
        ("new", Some(args)) => parma_new(args, &config),
        ("edit", Some(args)) => parma_edit(args, &config),
        ("import", Some(args)) => parma_import(args, &config),
        ("show", Some(args)) => parma_show(args, &config),
        ("tag", Some(args)) => parma_tag(args, &config),
        ("delete", Some(args)) => parma_delete(args, &config),
        ("render", Some(args)) => {
            let conf_resource_location = args.value_of("resource_location");

            if conf_resource_location.is_some() {
                config.resource_location = conf_resource_location.map(|o| o.to_owned());
            }

            parma_render(args, &config)
        }
        ("rofi", _) => parma_rofi(&config),
        ("from-legacy", Some(args)) => parma_from_legacy(args),
        ("ids", _) => parma_ids(&config),
        #[cfg(feature = "matrix")]
        ("sync", _) => parma_sync(&config),
        ("annotate", Some(args)) => parma_annotate(args, &config),
        ("search", Some(args)) => parma_search(args),
        ("menu", Some(args)) => parma_menu(args),

        //// Subcommand: query
        _ => match args.subcommand_name() {
            None => Err(anyhow!("No subcommand selected.")),

            Some(name) => Err(anyhow!("Unknown subcommand: {}", name)),
        },
    }
}
