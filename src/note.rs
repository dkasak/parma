use std::borrow::Cow;
use std::collections::HashMap;
use std::fmt;
use std::fmt::Debug;

use crypto::digest::Digest;
use crypto::sha1::Sha1;
use serde::{Deserialize, Serialize};
use serde_json::Value;
use thiserror::Error;

#[cfg(feature = "matrix")]
use matrix_sdk::events::{room::message::MessageEventContent, AnyMessageEventContent};
use tracing::{debug, trace};

use crate::util::date::ParmaDate;

/// The model of a note.
///
/// Each note is assigned an immutable ID which can be used to uniquely refer to that particular
/// note.
#[serde_with::skip_serializing_none]
#[derive(Deserialize, Serialize, Debug, Clone)]
pub struct Note<'a> {
    pub id: Cow<'a, str>,
    #[serde(flatten)]
    pub header: NoteHeader<'a>,
    pub text: Cow<'a, str>,
}

#[serde_with::skip_serializing_none]
#[derive(Deserialize, Serialize, Debug, Clone)]
#[serde(rename_all = "kebab-case")]
pub struct NoteHeader<'a> {
    pub author: Option<Cow<'a, str>>,
    pub date: Option<ParmaDate>,
    pub content_date: Option<ParmaDate>,
    pub content_author: Option<Cow<'a, str>>,
    pub topic: Option<Cow<'a, str>>,
    pub title: Option<Cow<'a, str>>,
    pub subtitle: Option<Cow<'a, str>>,

    #[serde(default)]
    #[serde(skip_serializing_if = "Vec::is_empty")]
    pub keywords: Vec<Cow<'a, str>>,

    /// Non-standard header fields are collected in this hash map.
    #[serde(default, flatten)]
    #[serde(skip_serializing_if = "HashMap::is_empty")]
    pub custom: HashMap<Cow<'a, str>, Cow<'a, Value>>,
}

impl<'a> Note<'a> {
    /// Splits a Markdown note (with YAML header) into a header and body. Removes HTML comments
    /// from the input, if any.
    ///
    /// # Example
    ///
    /// ```
    /// let note: String =
    /// r#"---
    /// - author: Foo Bar
    /// - title: Note title
    /// ...
    ///
    /// Note body.
    /// "#;
    ///
    /// let (header, body) = parse_header_body(note);
    ///
    /// assert_eq!(header,
    /// r#"---
    /// - author: Foo Bar
    /// - title: Note title
    /// ...
    /// "#);
    ///
    /// assert_eq!(body, "Note body.\n");
    /// ```
    pub fn parse_header_body(raw_note: &str) -> (String, String) {
        #[derive(PartialEq, Clone)]
        enum State {
            Start,
            Header,
            Body,
        }

        let mut state = State::Start;

        let mut skip_next_line_if_empty = false;
        let mut skip_header_delim = false;
        let mut seen_header = false;

        let mut header = String::new();
        let mut body = String::new();

        for line in raw_note.lines() {
            let old_state = state.clone();

            state = match line.trim() {
                "---" => match state {
                    State::Start => State::Header,
                    _ => State::Body,
                },

                "..." => match state {
                    State::Header => State::Body,
                    _ => State::Body,
                },

                // Whitespace must not have the ability to change state.
                "" => state,

                _ => match state {
                    State::Start => State::Body,
                    s => s,
                },
            };

            match (&old_state, &state) {
                (&State::Start, &State::Header) => {
                    skip_header_delim = true;
                    seen_header = true;
                }

                (&State::Header, &State::Body) => {
                    skip_header_delim = true;
                    skip_next_line_if_empty = true;
                }
                _ => (),
            }

            if skip_header_delim {
                skip_header_delim = false;
                continue;
            }

            if skip_next_line_if_empty {
                skip_next_line_if_empty = false;

                if line.trim().is_empty() {
                    continue;
                }
            }

            if state == State::Header {
                header += line;
                header += "\n";
            } else if state == State::Body {
                body += line;
                body += "\n";
            }
        }

        // If the header was present, we need to wrap it in delimiters again. Otherwise, we leave
        // it as an empty string.
        if seen_header {
            header = format!("---\n{}...\n", header);
        }

        (header, body)
    }

    /// Preprocess the header by applying some mangling to certain fields.
    ///
    /// Currently, this entails quoting some fields where we never expect arbitrary YAML,
    /// just ordinary strings. This is a hack so that we don't have to worry about special
    /// symbols (like the colon) in fields like the title when writing the note.
    ///
    /// # Example
    ///
    /// ```
    /// let header: String = r#"
    /// ---
    /// title: Note title
    /// ...
    /// "#;
    ///
    /// let header = preprocess_header(header);
    ///
    /// assert_eq!(header, r#"
    /// ---
    /// title: >-
    ///   Note title
    /// ...
    /// "#);
    /// ```
    fn preprocess_header(header: String) -> String {
        let mut preprocessed = String::new();

        // Fields which get preprocessed
        let mangled_fields = ["title"];

        for line in header.lines() {
            // XXX: Quote "title" field content since I always considered it pure text, but
            // haven't always made sure it doesn't contain YAML special characters
            let trimmed_line = line.trim();

            if let Some((header_field, header_value)) = trimmed_line.split_once(':') {
                let header_field = header_field.trim();
                let header_value = header_value.trim();

                if mangled_fields.iter().any(|field| *field == header_field)
                {
                    if !(header_value.chars().nth(0) == Some('"')
                        && header_value.chars().last() == Some('"')) {
                        preprocessed += &line.replacen(":", ": >-\n  ", 1);
                    } else {
                        preprocessed += &line;
                    }
                } else {
                    preprocessed += &line;
                }
            } else {
                preprocessed += &line;
            }

            preprocessed += "\n";
        }

        preprocessed
    }

    /// Produce a Note from raw Markdown (with a YAML header).
    pub fn from_markdown(raw_note: Cow<'a, str>) -> MarkdownParseResult<Note<'a>> {
        let mut sha1 = Sha1::new();
        sha1.input_str(&raw_note);
        let id = sha1.result_str().to_owned();

        Self::from_markdown_with_id(id.into(), raw_note)
    }

    /// Produce a Note from raw Markdown (with a YAML header), but supplying your own ID.
    pub fn from_markdown_with_id(
        id: Cow<'a, str>,
        raw_note: Cow<'a, str>,
    ) -> MarkdownParseResult<Note<'a>> {
        let (mut header, body) = Self::parse_header_body(&raw_note);
        header = Self::preprocess_header(header);

        let header = serde_yaml::from_str(&header).map_err(|err| {
            MarkdownNoteParseError::MalformedHeader {
                note_id: id.clone().into(),
                source: err,
            }
        })?;

        if body.is_empty() {
            Err(MarkdownNoteParseError::EmptyNote {
                note_id: id.clone().into(),
            })
        } else {
            Ok(Note {
                id: id.into(),
                header,
                text: Cow::Owned(body.trim_end().into()),
            })
        }
    }

    /// Produce a canonical Markdown (with YAML header) representation of the note.
    ///
    /// Also used for the `Display` implementation.
    pub fn to_markdown(&self) -> String {
        format!("{header}\n{body}", header = self.header, body = self.text)
    }

    /// Produce a Note from a JSON string representing the note.
    pub fn from_json(s: Cow<'a, str>) -> serde_json::Result<Note<'a>> {
        serde_json::from_str(&s.to_owned())
    }

    /// Produce a Note from a JSON value representing the note.
    pub fn from_json_value(v: Value) -> serde_json::Result<Note<'a>> {
        serde_json::from_value(v.clone())
    }

    pub fn add_tag(&mut self, tag: Cow<'a, str>) -> Option<Cow<'a, str>> {
        if !self.header.keywords.contains(&tag) {
            self.header.keywords.push(tag.to_owned());

            trace!("Adding tag {} to note {}", tag, self.id);

            Some(tag)
        } else {
            trace!(
                "Not adding tag {} to note {} since it already exists",
                tag,
                self.id
            );

            None
        }
    }

    pub fn remove_tag(&mut self, tag: Cow<'a, str>) -> Option<Cow<'a, str>> {
        if self.header.keywords.contains(&tag) {
            self.header.keywords.retain(|t| *t != tag);

            trace!("Removing tag {} from note {}", tag, self.id);

            Some(tag)
        } else {
            trace!(
                "Attempted removing non-existent tag {} from note {}",
                tag,
                self.id
            );

            None
        }
    }
}

impl<'a> fmt::Display for Note<'a> {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "{}", self.to_markdown())
    }
}

impl<'a> fmt::Display for NoteHeader<'a> {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "{}...\n", serde_yaml::to_string(self).unwrap())
    }
}

/// Represents a parsing error when parsing a JSON note.
#[derive(Debug, Error)]
#[error("Failed parsing JSON note")]
pub enum JsonNoteParseError {
    ParseError {
        note_id: String,
        source: serde_yaml::Error,
    },
}

/// Represents a parsing error when parsing a Markdown note.
#[derive(Debug, Error)]
pub enum MarkdownNoteParseError {
    #[error("Note {note_id} has malformed header: {source}")]
    MalformedHeader {
        note_id: String,
        #[source]
        source: serde_yaml::Error,
    },
    #[error("Note {note_id} is empty")]
    EmptyNote { note_id: String },
}

impl PartialEq for MarkdownNoteParseError {
    fn eq(&self, other: &Self) -> bool {
        if let Self::MalformedHeader { .. } = self {
            if let Self::MalformedHeader { .. } = other {
                true
            } else {
                false
            }
        } else if let Self::EmptyNote { .. } = self {
            if let Self::EmptyNote { .. } = other {
                true
            } else {
                false
            }
        } else {
            false
        }
    }
}

type MarkdownParseResult<T> = std::result::Result<T, MarkdownNoteParseError>;

/// Represents a conversion error when converting a note to a Matrix event.
#[derive(Debug, Error)]
pub enum NoteToMatrixEventConversionError {
    #[error("Original event isn't able to be converted to JSON.")]
    OriginalEventToJson,
    #[error("JSON cannot be converted to object.")]
    JsonToObject,
    #[error("Modified node with extra metadata cannot be converted back to object.")]
    ModifiedNoteBackToObject,
}

#[cfg(feature = "matrix")]
impl<'a> From<&Note<'a>> for AnyMessageEventContent {
    fn from(note: &Note) -> Self {
        let matrix_message_body = format!("# Note {}\n\n{}", note.id, note.text);

        // TODO: Once we have Markdown -> Matrix HTML conversion, we should use ::html instead
        // so we get a nice note rendering in Element and other clients.
        let content = AnyMessageEventContent::RoomMessage(MessageEventContent::text_plain(
            matrix_message_body,
        ));

        let res: Result<BTreeMap<String, Value>, NoteToMatrixEventConversionError> = try {
            let content_json = serde_json::to_value(content)
                .map_err(|_| NoteToMatrixEventConversionError::OriginalEventToJson)?;
            let mut content_map: BTreeMap<String, Value> = serde_json::from_value(content_json)
                .map_err(|_| NoteToMatrixEventConversionError::JsonToObject)?;
            content_map.insert(
                "parma".to_owned(),
                serde_json::to_value(note)
                    .map_err(|_| NoteToMatrixEventConversionError::ModifiedNoteBackToObject)?,
            );

            content_map
        };

        if let Ok(res) = res {
            matrix_sdk::events::AnyMessageEventContent::_Custom(
                matrix_sdk::events::custom::CustomEventContent {
                    event_type: "m.room.message".to_owned(),
                    data: res.into(),
                },
            )
        } else {
            panic!("Couldn't convert note to Matrix message");
        }
    }
}

#[cfg(test)]
mod test {
    use super::*;

    use indoc::indoc;
    use std::borrow::Cow;

    use crate::assert_pat;

    static NOTE_EMPTY: &str = "";

    static NOTE_EMPTY_ONLY_WHITESPACE: &str = " \n ";

    static NOTE_NO_HEADER: &str = "Quux.";

    static NOTE_EMPTY_HEADER: &str = indoc!(
        "---
         ...

         Foo."
    );

    static NOTE_EMPTY_BODY: &str = indoc!(
        "---
         title: Foo
         ...

        "
    );

    static NOTE_MULTILINE_TEXT: &str = indoc!(
        "---
         title: Foo
         ...

         A
         B
         C"
    );

    static NOTE_NO_EMPTY_LINE_AFTER_HEADER: &str = indoc!(
        "---
         title: Foo
         ...
         Foo."
    );

    static NOTE_WITH_EMPTY_LINE_BEFORE_HEADER: &str = indoc!(
        // Warning: important whitespace below
        "
         ---
         title: Foo
         ...

         Foo."
    );

    static NOTE_WITH_ELLIPSIS_IN_BODY: &str = indoc!(
        "---
        title: Foo
        ...

        ...
        Foo."
    );

    static NOTE_HAPPY_PATH: &str = indoc!(
        "---
         author: Fëanor
         date: 1391-07-12
         content-author: Rúmil
         topic: tengwar
         title: Sarati
         keywords:
           - writing
         type: commentary
         ...

         Tengwar > Sarati."
    );

    static NOTE_WITH_MULTIPLE_PARAGRAPHS: &str = indoc!(
        "---
         title: Foo
         ...

         A

         B

         C"
    );

    static NOTE_WITH_INVALID_DATE: &str = indoc!(
        "---
         date: 2019-13-01
         ...

         Foo.
        "
    );

    static NOTE_WITH_CUSTOM_HEADER_FIELDS: &str = indoc!(
        "---
         date: 2020-01-01
         some-custom-field: foo
         ...

         Foo.
        "
    );

    static NOTE_TRAILING_WHITESPACE: &str = indoc!(
        "---
         date: 2020-01-01
         ...

         Foo is bar.



        "
    );

    mod note_from_markdown {
        use super::*;
        use std::str::FromStr;

        #[test]
        pub fn multiline_note_is_parsed_correctly() {
            let note = Note::from_markdown(Cow::Borrowed(NOTE_MULTILINE_TEXT))
                .ok()
                .unwrap();

            assert_eq!(note.id, "c357df098a41ea8f36c493a263c77944a84c3ca5");

            let header = &note.header;

            assert_eq!(header.title, Some(Cow::Borrowed("Foo")));
            assert_eq!(note.text, "A\nB\nC");
        }

        #[test]
        pub fn happy_path_note_is_parsed_correctly() {
            let note = Note::from_markdown(Cow::Borrowed(NOTE_HAPPY_PATH))
                .ok()
                .unwrap();

            assert_eq!(note.id, "9e5997b0bbd5712dafd8fa18871b41b8f59a1604");

            let header = &note.header;

            assert_eq!(header.author, Some(Cow::Borrowed("Fëanor")));
            assert_eq!(header.content_author, Some(Cow::Borrowed("Rúmil")));
            assert_eq!(header.date, ParmaDate::from_str("1391-07-12").ok());
            assert_eq!(header.title, Some(Cow::Borrowed("Sarati")));
            assert_eq!(header.content_date, None);
            assert_eq!(header.topic, Some(Cow::Borrowed("tengwar")));
            assert_eq!(header.keywords, vec![Cow::Borrowed("writing")]);
            assert_eq!(note.text, "Tengwar > Sarati.");
        }

        #[test]
        pub fn trailing_whitespace_is_removed_from_the_note() {
            let note = Note::from_markdown(Cow::Borrowed(NOTE_TRAILING_WHITESPACE))
                .ok()
                .unwrap();

            assert_eq!(note.text, "Foo is bar.");
        }

        #[test]
        pub fn empty_string_produces_malformed_header_error() {
            let note = Note::from_markdown(Cow::Borrowed(NOTE_EMPTY));

            assert!(note.is_err());
            assert_pat!(
                note.unwrap_err(),
                MarkdownNoteParseError::MalformedHeader { .. }
            );
        }

        #[test]
        pub fn whitespace_only_string_produces_malformed_header_error() {
            let note = Note::from_markdown(Cow::Borrowed(NOTE_EMPTY_ONLY_WHITESPACE));

            assert!(note.is_err());
            assert_pat!(
                note.unwrap_err(),
                MarkdownNoteParseError::MalformedHeader { .. }
            );
        }

        // XXX: Should work but doesn't. See <https://github.com/dtolnay/serde-yaml/issues/86>.
        //
        // #[test]
        // pub fn note_with_empty_header_is_okay() {
        //     let note = Note::from_markdown(Cow::Borrowed(NOTE_EMPTY_HEADER));

        //     assert!(note.is_ok());
        //     assert_eq!(note.unwrap().text, "Foo.");
        // }

        #[test]
        pub fn empty_body_produces_empty_note_error() {
            let note = Note::from_markdown(Cow::Borrowed(NOTE_EMPTY_BODY));

            assert!(note.is_err());
            assert_pat!(note.unwrap_err(), MarkdownNoteParseError::EmptyNote { .. });
        }

        #[test]
        pub fn note_with_invalid_date() {
            let note = Note::from_markdown(Cow::Borrowed(NOTE_WITH_INVALID_DATE));

            assert_pat!(note, Err(MarkdownNoteParseError::MalformedHeader { .. }));
        }

        #[test]
        pub fn custom_header_fields_are_collected_in_hashmap() -> Result<(), String> {
            let note = Note::from_markdown(Cow::Borrowed(NOTE_WITH_CUSTOM_HEADER_FIELDS))
                .map_err(|e| format!("Failed parsing note: {}", e))?;

            let header = &note.header;

            assert_eq!(
                header.custom.get("some-custom-field"),
                Some(&Cow::Borrowed("foo"))
            );
            assert_eq!(header.custom.get("nonexistent-custom-field"), None);
            assert_eq!(header.custom.get("date"), None);

            Ok(())
        }

        #[test]
        pub fn fields_are_quoted_unless_already_quoted() {
            let header: String = indoc!(
                "
            ---
            title: Note title
            ...
            "
            )
            .to_owned();

            let header = Note::preprocess_header(header);

            assert_eq!(
                header,
                indoc!(
                    "
            ---
            title: >-
               Note title
            ...
            "
                )
            );

            let header2: String = indoc!(
                "
            ---
            title: \"Already quoted\"
            ...
            "
            )
            .to_owned();

            let header2 = Note::preprocess_header(header2);

            assert_eq!(header2, header2);
        }
    }

    mod split_header_body {
        use super::*;

        #[test]
        pub fn empty_note_splits_into_empty_header_and_body() {
            let (header, body) = Note::parse_header_body(NOTE_EMPTY);

            assert_eq!(header, "");
            assert_eq!(body, "");
        }

        #[test]
        pub fn missing_header_produces_empty_string_as_header() {
            let (header, body) = Note::parse_header_body(NOTE_NO_HEADER);

            assert_eq!(header, "");
            assert_eq!(body, "Quux.\n");
        }

        #[test]
        pub fn no_body_with_empty_header_produces_empty_header() {
            let (header, body) = Note::parse_header_body(NOTE_EMPTY_BODY);

            assert_eq!(header, "---\ntitle: Foo\n...\n");
            assert_eq!(body, "");
        }

        #[test]
        pub fn empty_header_is_parsed_correctly() {
            let (header, body) = Note::parse_header_body(NOTE_EMPTY_HEADER);

            assert_eq!(header, "---\n...\n");
            assert_eq!(body, "Foo.\n");
        }

        #[test]
        pub fn ellipsis_in_body_is_okay() {
            let (header, body) = Note::parse_header_body(NOTE_WITH_ELLIPSIS_IN_BODY);

            assert_eq!(header, "---\ntitle: Foo\n...\n");
            assert_eq!(body, "...\nFoo.\n");
        }

        #[test]
        pub fn no_empty_line_after_header_is_okay() {
            let (header, body) = Note::parse_header_body(NOTE_NO_EMPTY_LINE_AFTER_HEADER);

            assert_eq!(header, "---\ntitle: Foo\n...\n");
            assert_eq!(body, "Foo.\n");
        }

        #[test]
        pub fn empty_line_before_header_is_ignored() {
            let (header, body) = Note::parse_header_body(NOTE_WITH_EMPTY_LINE_BEFORE_HEADER);

            assert_eq!(header, "---\ntitle: Foo\n...\n");
            assert_eq!(body, "Foo.\n");
        }

        #[test]
        pub fn paragraphs_are_preserved() {
            let (_, body) = Note::parse_header_body(NOTE_WITH_MULTIPLE_PARAGRAPHS);

            assert_eq!(body, "A\n\nB\n\nC\n");
        }
    }

    mod laws {
        use super::*;

        #[test]
        pub fn from_markdown_to_markdown_is_identity() {
            let json_note = Note::from_markdown(NOTE_HAPPY_PATH.into()).ok().unwrap();
            let markdown_note = format!("{}", &json_note);

            assert_eq!(markdown_note, NOTE_HAPPY_PATH);
        }
    }
}
