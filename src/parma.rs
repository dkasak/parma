use crate::note::Note;
use crate::storage::json::{load_from_file, JsonStorageError};

use std::path::Path;

use tracing::info;
use thiserror::Error;

pub struct Parma<'a> {
    notes: Vec<Note<'a>>,
}

impl<'a> Parma<'a> {
    pub fn from_file(notes_path: &'a Path) -> Result<Parma, ParmaError> {
        if !notes_path.is_file() {
            std::fs::write(notes_path, "[]\n").ok().ok_or(ParmaError::FailedCreatingNotesFile)?;
        }

        Ok(Parma {
            notes: load_from_file(notes_path)?,
        })
    }

    pub fn note_with_id(&mut self, note_id: &str) -> Option<&mut Note<'a>> {
        for n in self.notes.iter_mut() {
            if n.id == note_id {
                return Some(n);
            }
        }

        None
    }

    pub fn delete(&mut self, note_id: &str) -> Result<(), ParmaError> {
        self.notes.remove(
            self.notes
                .iter()
                .position(|n| n.id == note_id)
                .ok_or(ParmaError::DeleteOfNonexistentNote)?,
        );

        Ok(())
    }

    pub fn insert(&mut self, note: Note<'a>) -> Result<(), ParmaError> {
        for n in self.notes.iter_mut() {
            if n.id == note.id {
                return Err(ParmaError::DuplicateNote);
            }
        }

        self.notes.push(note);

        Ok(())
    }

    pub fn store(&mut self, path: &Path) -> Result<(), ParmaError> {
        info!("Storing notes at {}", path.display());

        Ok(std::fs::write(
            path,
            serde_json::to_string_pretty(&self.notes).unwrap(),
        )?)
    }

    pub fn iter(&self) -> std::slice::Iter<Note<'a>> {
        self.notes.iter()
    }
}

impl<'a> IntoIterator for Parma<'a> {
    type Item = Note<'a>;
    type IntoIter = std::vec::IntoIter<Self::Item>;

    fn into_iter(self) -> Self::IntoIter {
        self.notes.into_iter()
    }
}

#[derive(Error, Debug)]
pub enum ParmaError {
    #[error("Error creating notes files. Wrong directory or permissions?")]
    FailedCreatingNotesFile,
    #[error("Error loading notes from storage")]
    FailedLoadingNotes(#[from] JsonStorageError),
    #[error("Error saving notes to storage")]
    FailedStoringNotes(#[from] std::io::Error),
    #[error("A note with the same ID already exists")]
    DuplicateNote,
    #[error("Attempted deletion of nonexistent note")]
    DeleteOfNonexistentNote,
}
