use std::path::PathBuf;

use pandoc::{Pandoc, PandocError, PandocOption, PandocOutput};

use crate::config::Config;
use crate::note::Note;

pub fn to_html(
    args: &clap::ArgMatches,
    config: &Config,
    note: &Note,
) -> Result<PandocOutput, PandocError> {
    let mut pandoc = Pandoc::new();

    let parma_data_dir = Config::data_dir();
    let style_dir = parma_data_dir.join("style");
    let resource_location = config.resource_location.as_ref().unwrap();

    let mut pandoc_args: Vec<PandocOption> = vec![
        PandocOption::MathJax(None),
        PandocOption::ShiftHeadingLevelBy(1),
        PandocOption::SectionDivs,
        PandocOption::Template(PathBuf::from(style_dir.join("tufte.html5"))),
        PandocOption::Filter(PathBuf::from("pandoc-sidenote")),
        PandocOption::Css(format!("{}/{}", resource_location, "tufte.css")),
        PandocOption::Css(format!("{}/{}", resource_location, "tufte-extra.css")),
        PandocOption::Css(format!("{}/{}", resource_location, "pandoc.css")),
    ];

    if args.is_present("standalone") {
        pandoc_args.push(PandocOption::Standalone);
    }

    pandoc.add_options(&pandoc_args);

    pandoc.set_input(pandoc::InputKind::Pipe(note.to_markdown()));
    pandoc.set_input_format(pandoc::InputFormat::Markdown, vec![]);
    pandoc.set_output(pandoc::OutputKind::Pipe);
    pandoc.set_output_format(pandoc::OutputFormat::Html5, vec![]);

    pandoc.execute()
}

pub fn to_pdf(config: &Config, note: &Note) -> Result<PandocOutput, PandocError> {
    let mut pandoc = Pandoc::new();

    let resource_location = config.resource_location.as_ref().unwrap();

    pandoc.add_options(&[
        PandocOption::MathJax(None),
        PandocOption::SectionDivs,
        PandocOption::Template(PathBuf::from(format!(
            "{}/{}",
            resource_location, "tufte-handout.tex"
        ))),
        PandocOption::IncludeInHeader(PathBuf::from(format!(
            "{}/{}",
            resource_location, "tufte-handout-custom.tex"
        ))),
        PandocOption::Var("documentclass".to_owned(), Some("tufte-handout".to_owned())),
        PandocOption::Standalone,
    ]);

    pandoc.set_input(pandoc::InputKind::Pipe(note.to_markdown()));
    pandoc.set_input_format(pandoc::InputFormat::Markdown, vec![]);
    pandoc.set_output(pandoc::OutputKind::Pipe);
    pandoc.set_output_format(pandoc::OutputFormat::Pdf, vec![]);

    pandoc.execute()
}
