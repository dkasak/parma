use crate::note::Note;
use itertools::Itertools;

pub fn note_to_rofi(note: &Note) -> String {
    let text_field = format!("{} ", note.text.split_whitespace().format(" "));
    let id_field = format!("id:{}", note.id);

    let header = &note.header;

    let (leading_topic_field, topic_field) = if let Some(topic) = &header.topic {
        (format!("|{}| ", topic), format!("+{} ", topic))
    } else {
        ("".into(), "".into())
    };

    let title_field = match &header.title {
        Some(title) => format!("{}{} ", title, if title.ends_with('?') { "" } else { ":" }),
        None => "".into(),
    };

    let keywords_field = format!(
        "{}{}",
        header
            .keywords
            .iter()
            .map(|k| format!("#{}", k))
            .collect::<Vec<String>>()
            .join(" "),
        if header.keywords.is_empty() { "" } else { " " }
    );

    let date_field = format!(
        "{}{}",
        match &header.date {
            Some(date) => format!("date:{} ", date),
            None => "".into(),
        },
        match &header.content_date {
            Some(content_date) => format!("content-date:{} ", content_date),
            None => "".into(),
        }
    );

    let author_field = format!(
        "{}{}",
        match &header.author {
            Some(author) => format!("%{}% ", author),
            None => "".into(),
        },
        match &header.content_author {
            Some(content_author) => format!("~{}~ ", content_author),
            None => "".into(),
        },
    );

    let custom_field = format!(
        "{}{}",
        header
            .custom
            .iter()
            .map(|(field, value)| format!("{}:{}", field, value))
            .collect::<Vec<String>>()
            .join(" "),
        if header.custom.is_empty() { "" } else { " " }
    );

    format!(
        concat!(
            "{leading_topic_field}{title_field}{text_field}| ",
            "{date_field}{topic_field}{keywords_field}{author_field}{custom_field}| ",
            "{id_field}"
        ),
        leading_topic_field = leading_topic_field,
        topic_field = topic_field,
        title_field = title_field,
        text_field = text_field,
        date_field = date_field,
        keywords_field = keywords_field,
        author_field = author_field,
        custom_field = custom_field,
        id_field = id_field,
    ).replace('\n', "")
}
