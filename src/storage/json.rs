//! # JSON storage format parser
//!
//! Functionality related to loading, writing and manipulating collections of notes from JSON
//! storage format.

use thiserror::Error;

use std::fs;
use std::path::Path;

use crate::note::Note;

#[derive(Error, Debug)]
pub enum JsonStorageError {
    #[error("Failure parsing JSON: {0:#?}")]
    JsonError(#[from] serde_json::error::Error),
    #[error("Cannot read file: {0}")]
    IOError(#[from] std::io::Error),
}

pub fn load_from_str(json: &str) -> Result<Vec<Note>, JsonStorageError> {
    let notes_vec = serde_json::from_str(json)?;
    Ok(notes_vec)
}

pub fn load_from_string<'a>(json: String) -> Result<Vec<Note<'a>>, JsonStorageError> {
    let notes_vec = serde_json::from_str(&json)?;
    Ok(notes_vec)
}

pub fn load_from_file(file_path: &Path) -> Result<Vec<Note>, JsonStorageError> {
    let notes_json = fs::read_to_string(file_path)?;
    load_from_string(notes_json)
}
