//! # Legacy storage format parser
//!
//! This module implements a parser of the legacy note storage format. The old format is basically
//! a large plain text file consisting of note after note. Each note consists of its content,
//! followed by the string `" | id:"`, followed by the hash of the note's content, followed by
//! a newline.
//!
//! Historical note: this format was a multi-line variant of the format used by the [t
//! task-manager](https://github.com/sjl/t/) which I initially used for short notes.

use regex::Regex;
use thiserror::Error;

/// The parser for the legacy note storage.
pub(crate) struct Legacy {
    content: String,
}

#[derive(PartialEq, Debug, Error)]
pub(crate) enum LegacyParserError {
    #[error("The last note has missing or malformed metadata.")]
    LastNoteMissingMetadata,
}

impl Legacy {
    pub(crate) fn new() -> Self {
        Legacy {
            content: String::new(),
        }
    }

    /// Streaming interface of the legacy note storage parser.
    ///
    /// Each call feeds a portion of the storage file. Once at least one full note is fed, produce
    /// a vector of pairs (note content, note ID).
    ///
    /// Since `feed` works as a streaming parser, it cannot detect errors such as the last note
    /// missing an ID. To check for this, call `done` once you've input the entire storage file.
    pub(crate) fn feed(&mut self, portion: &str) -> Vec<(String, String)> {
        let id_regex = Regex::new(" \\| id:([a-fA-F0-9]{40})").unwrap();

        let mut pairs = Vec::new();

        for line in portion.split('\n') {
            if let Some(cap) = id_regex.captures_iter(line).next() {
                // delete note ID
                let line = id_regex.replace(&line, "");

                // append last line to current content
                self.content += &line;
                self.content += "\n";

                pairs.push((cap[1].to_owned(), self.content.clone()));

                self.reset();
            } else {
                self.content += &line;
                self.content += "\n";
            }
        }

        pairs
    }

    /// Non-streaming version of the legacy format storage parser.
    ///
    /// `input`: the complete contents of a legacy storage file
    pub(crate) fn parse(
        &mut self,
        input: &str,
    ) -> Result<Vec<(String, String)>, LegacyParserError> {
        let notes = self.feed(input);

        if let Err(e) = self.done() {
            Err(e)
        } else {
            Ok(notes)
        }
    }

    /// Indicate that the whole storage file has been fed and return an error if it was malformed.
    pub(crate) fn done(&self) -> Result<(), LegacyParserError> {
        if self.content.trim().is_empty() {
            Ok(())
        } else {
            Err(LegacyParserError::LastNoteMissingMetadata)
        }
    }

    /// Reset the parser.
    pub(crate) fn reset(&mut self) {
        self.content.clear();
    }
}

#[cfg(test)]
mod test {
    use std::format;

    use super::*;
    use crate::Legacy;

    #[test]
    pub fn parse_single_note() {
        let mut parser = Legacy::new();

        let line1 = "The first line of the note.";
        let line2 = "The second line of the note.";
        let note_id = "ead38cce5770b715e65a77ddf367febc99780e3d".into();
        let metadata = format!(" | id:{}", note_id);
        let line2_full = format!("{}{}", line2, metadata);

        let note_content = format!("{}\n{}\n", line1, line2);

        assert_eq!(parser.feed(line1), vec![]);
        assert_eq!(parser.feed(&line2_full), vec![(note_id, note_content)]);
    }

    #[test]
    pub fn parse_two_notes() {
        let mut parser = Legacy::new();

        let note1: String = "A".into();
        let note2: String = "B".into();
        let note1_id: String = "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa".into();
        let note2_id: String = "bbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbb".into();
        let note1_metadata = format!(" | id:{}", note1_id);
        let note2_metadata = format!(" | id:{}", note2_id);
        let note1_with_metadata = format!("{}{}", note1, note1_metadata);
        let note2_with_metadata = format!("{}{}", note2, note2_metadata);

        assert_eq!(
            parser.feed(&note1_with_metadata),
            vec![(note1_id, note1 + "\n")]
        );
        assert_eq!(
            parser.feed(&note2_with_metadata),
            vec![(note2_id, note2 + "\n")]
        );
    }

    #[test]
    pub fn feed_then_concat_is_concat_then_feed() {
        let mut parser = Legacy::new();

        let note1: String = "A".into();
        let note2: String = "B".into();
        let note1_id: String = "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa".into();
        let note2_id: String = "bbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbb".into();
        let note1_metadata = format!(" | id:{}", note1_id);
        let note2_metadata = format!(" | id:{}", note2_id);
        let note1_with_metadata = format!("{}{}", note1, note1_metadata);
        let note2_with_metadata = format!("{}{}", note2, note2_metadata);

        let concat = format!("{}\n{}\n", note1_with_metadata, note2_with_metadata);
        let concat_then_feed = parser.feed(&concat);

        parser.reset();

        let feed1_result = parser.feed(&note1_with_metadata);
        let feed2_result = parser.feed(&note2_with_metadata);
        let feed_then_concat = feed1_result
            .iter()
            .cloned()
            .chain(feed2_result.iter().cloned())
            .collect::<Vec<(String, String)>>();

        assert_eq!(concat_then_feed, feed_then_concat);
    }

    #[test]
    pub fn last_note_without_metadata_is_an_error() {
        let mut parser = Legacy::new();

        let note_without_metadata = "foo\n";

        // Incomplete note was fed, so the return vector is empty.
        assert_eq!(parser.feed(note_without_metadata), vec![]);

        // Calling done results in an error because the last note is missing metadata.
        assert_eq!(
            parser.done(),
            Err(LegacyParserError::LastNoteMissingMetadata)
        );
    }
}
