pub static DEFAULT_TEMPLATE: &'static str = "---
author: {author}
date: {date}{{ if title }}
title: {title}{{ endif }}
...

{initial_text}
";

pub static ANNOTATION_TEMPLATE: &'static str = "---
author: {author}
date: {date}{{ if title }}
title: {title}{{ endif }}
annotation-for: {annotation_for}{{ if annotation_line }}
annotation-line: {annotation_line}{{ endif }}{{ if annotation_column }}
annotation-column: {annotation_column}{{ endif }}
...

{initial_text}
";

