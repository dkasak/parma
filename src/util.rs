use anyhow::{Context, Result};
use regex::Regex;
use tempfile::{Builder, NamedTempFile};

#[macro_export]
macro_rules! error {
    ( $message:expr, $code:expr ) => {{
        use std::process::exit;
        println!("Error: {}", $message);
        exit($code);
    }};
}

#[macro_export]
macro_rules! assert_pat {
    ($value:expr, $pattern:pat) => {{
        let value = &$value;

        if let $pattern = value {
        } else {
            panic!(
                r#"

assertion failed (value doesn't match pattern):

    value:  `{:#?}`,
 expected:  `{}`

"#,
                value,
                stringify!($pattern)
            )
        }
    }};
}

/// Remove leading HTML comments from the input.
///
/// HTML comments are used for providing feedback to the user via the editor, for example when the
/// note provided as input is invalid. We don't want these comments to be included in the input,
/// but we also don't want to burden the user with having to delete them.  This is the purpose of
/// this method.
pub fn remove_leading_html_comments<'a>(input: &'a str) -> String {
    let html_comments = Regex::new(r"(?sm)\s*<!--.*-->\n*").unwrap();

    if let Some(mat) = html_comments.find(input) {
        if mat.start() == 0 {
            html_comments.replace_all(input, "").into_owned()
        } else {
            input.to_owned()
        }
    } else {
        input.to_owned()
    }
}

/// Helper module for creation of temporary files.
pub fn temp_markdown_file() -> Result<NamedTempFile> {
    Builder::new()
        .prefix("parma-")
        .suffix(".md")
        .tempfile()
        .context("Failed opening temporary file")
}

/// Helper module for interaction with the user via a text editor.
pub mod editor {
    use anyhow::{Context, Result};
    use std::path::Path;
    use std::process::Command;

    use crate::config::Config;

    /// Spawn a text editor with `file_path` opened.
    pub fn edit(config: &Config, file_path: &Path) -> Result<()> {
        let editor = config.editor().context(
            "Editor not configured (neither in config file nor set via EDITOR environment variable)",
        )?;

        let terminal = config
            .terminal
            .as_ref()
            .context("Terminal not configured in the config file")?;
        let termina_fname = Path::new(terminal)
            .file_name()
            .context("Invalid editor command, check your config.")?
            .to_str()
            .context(
                "Non-UTF8 characters used as part of the editor command, check your config.",
            )?;

        let mut editor_cmd: Vec<&str> = Vec::new();

        if editor.in_terminal {
            match termina_fname {
                "gnome-terminal" => {
                    editor_cmd.extend([terminal, "--wait", "--", &editor.command].iter())
                }
                _ => editor_cmd.extend([terminal, "--", &editor.command].iter()),
            }
        } else {
            editor_cmd.push(&editor.command);
        }

        let file_path_str = file_path.to_string_lossy();
        editor_cmd.push(file_path_str.as_ref());

        Command::new(editor_cmd[0])
            .args(editor_cmd[1..].iter())
            .spawn()
            .and_then(|mut c| c.wait())
            .context("Failed spawning editor")?;

        Ok(())
    }

    /// Prepend some text to a file.
    pub fn prepend(text: &str, file_path: &Path) -> Result<()> {
        let mut file_content = std::fs::read_to_string(file_path)?;
        file_content = format!("{}{}", text, file_content);
        std::fs::write(file_path, file_content)?;

        Ok(())
    }
}

pub mod date {
    use chrono::prelude::{DateTime, NaiveDate, Utc};
    use serde::de::{Deserializer, Visitor};
    use serde::ser::Serializer;
    use serde::{Deserialize, Serialize};
    use std::fmt;
    use std::str::FromStr;
    use std::time::SystemTime;

    /// Returns the current date in ISO8601 format as a `String`.
    pub fn today_as_string() -> String {
        let dt: DateTime<Utc> = SystemTime::now().into();
        format!("{}", dt.format("%Y-%m-%d"))
    }

    /// A special date type that allows encoding dates at different levels of specificity:
    ///
    /// 1. Year only: `2019`
    /// 2. Year and month: `2019-09`
    /// 3. Full date: `2019-09-11`
    #[derive(Debug, PartialEq, Clone)]
    pub enum ParmaDate {
        Year(i32),
        YearMonth(i32, u32),
        FullDate(NaiveDate),
    }

    impl ParmaDate {
        #[allow(dead_code)]
        pub fn to_naive_date(&self) -> NaiveDate {
            match self {
                ParmaDate::Year(y) => NaiveDate::from_ymd(*y, 0, 0),
                ParmaDate::YearMonth(y, m) => NaiveDate::from_ymd(*y, *m, 0),
                ParmaDate::FullDate(d) => *d,
            }
        }

        #[allow(dead_code)]
        pub fn to_string(&self) -> String {
            format!("{}", self)
        }
    }

    impl fmt::Display for ParmaDate {
        fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
            match self {
                Self::Year(y) => write!(f, "{}", y),
                Self::YearMonth(y, m) => write!(f, "{}-{}", y, m),
                Self::FullDate(d) => write!(f, "{}", d),
            }
        }
    }

    impl FromStr for ParmaDate {
        type Err = ParseParmaDateError;

        fn from_str(s: &str) -> Result<Self, Self::Err> {
            let s = s.trim();

            let fields: Vec<&str> = s.split('-').collect();

            match &fields[..] {
                [_y, _m, _d] => Ok(ParmaDate::FullDate(
                    NaiveDate::parse_from_str(s, "%Y-%m-%d").map_err(|_| ParseParmaDateError)?,
                )),
                [y, m] => {
                    let y = y.parse::<i32>().map_err(|_| ParseParmaDateError)?;
                    let m = m.parse::<u32>().map_err(|_| ParseParmaDateError)?;

                    if m > 12 {
                        Err(ParseParmaDateError)
                    } else {
                        Ok(ParmaDate::YearMonth(y, m))
                    }
                }
                [y] => {
                    let y = y.parse::<i32>().map_err(|_| ParseParmaDateError)?;
                    Ok(ParmaDate::Year(y))
                }
                _ => Err(ParseParmaDateError),
            }
        }
    }

    impl<'de> Deserialize<'de> for ParmaDate {
        fn deserialize<D>(deserializer: D) -> Result<Self, D::Error>
        where
            D: Deserializer<'de>,
        {
            struct ParmaDateVisitor;

            impl<'de> Visitor<'de> for ParmaDateVisitor {
                type Value = ParmaDate;

                fn expecting(&self, formatter: &mut fmt::Formatter) -> fmt::Result {
                    formatter.write_str(concat!(
                        "valid year (e.g. 2020), year and month ",
                        "(e.g. 2020-04) or full date (e.g. 2019-04-25)"
                    ))
                }

                fn visit_str<E>(self, value: &str) -> Result<ParmaDate, E>
                where
                    E: serde::de::Error,
                {
                    Self::Value::from_str(value)
                        .map_err(|_| E::invalid_value(serde::de::Unexpected::Str(value), &self))
                }
            }

            deserializer.deserialize_str(ParmaDateVisitor)
        }
    }

    impl Serialize for ParmaDate {
        fn serialize<S>(&self, serializer: S) -> Result<S::Ok, S::Error>
        where
            S: Serializer,
        {
            serializer.serialize_str(&format!("{}", self))
        }
    }

    #[derive(Debug, PartialEq)]
    pub struct ParseParmaDateError;

    #[cfg(test)]
    mod test {
        use super::*;

        #[test]
        pub fn year_string_parses_to_year_variant() {
            let date = ParmaDate::from_str("2020");

            assert_eq!(date, Ok(ParmaDate::Year(2020)));
        }

        #[test]
        pub fn year_month_string_parses_to_year_month_variant() {
            let date = ParmaDate::from_str("2020-01");

            assert_eq!(date, Ok(ParmaDate::YearMonth(2020, 01)));
        }

        #[test]
        pub fn ymd_string_parses_to_full_date_variant() {
            let date_str = "2020-01-01";
            let date = ParmaDate::from_str(&date_str);

            assert_eq!(
                date,
                Ok(ParmaDate::FullDate(
                    NaiveDate::parse_from_str(date_str, "%Y-%m-%d").unwrap()
                ))
            );
        }

        #[test]
        pub fn invalid_dates_fail_to_parse() {
            assert!(ParmaDate::from_str("a").is_err());
            assert!(ParmaDate::from_str("2019-").is_err());
            assert!(ParmaDate::from_str("2020-01-").is_err());
            assert!(ParmaDate::from_str("2020-01-01a").is_err());
            assert!(ParmaDate::from_str("2020-01-01-").is_err());
            assert!(ParmaDate::from_str("2020 -01-01").is_err());
        }

        #[test]
        pub fn out_of_range_month_fails_to_parse() {
            assert!(ParmaDate::from_str("2020-13").is_err());
            assert!(ParmaDate::from_str("2020-13-01").is_err());
        }

        #[test]
        pub fn out_of_range_day_fails_to_parse() {
            assert!(ParmaDate::from_str("2020-01-42").is_err());
            assert!(ParmaDate::from_str("2020-04-31").is_err());
            assert!(ParmaDate::from_str("2019-02-29").is_err());
        }

        #[test]
        pub fn whitespace_around_date_is_okay() {
            assert!(ParmaDate::from_str(" 2020 ").is_ok());
            assert!(ParmaDate::from_str(" 2020-01 ").is_ok());
            assert!(ParmaDate::from_str(" 2020-01-01 ").is_ok());
        }

        #[test]
        pub fn leap_years_are_accounted_for() {
            let date_str = "2020-02-29";
            let date = ParmaDate::from_str(date_str);

            assert_eq!(
                date,
                Ok(ParmaDate::FullDate(NaiveDate::from_ymd(2020, 2, 29)))
            );
        }
    }
}

#[cfg(test)]
mod test {
    use super::*;
    use indoc::*;

    static TEXT_WITH_LEADING_COMMENT: &str = indoc!(
        "<!-- This is a comment that should
              be removed.
         -->
        foo"
    );

    static TEXT_WITH_LEADING_COMMENT_AND_SPACES: &str = indoc!(
        "<!-- This is a comment that should
              be removed.
         -->


        foo"
    );

    static TEXT_WITH_TRAILING_COMMENT: &str = indoc!(
        "foo
         <!-- This is a comment that should
              be removed.
         -->
        "
    );

    static TEXT_WITH_INFIX_COMMENT: &str = indoc!(
        "foo
         <!-- This is a comment that should
              be removed.
         -->
         bar"
    );

    #[test]
    pub fn leading_comments_are_removed() {
        let text = remove_leading_html_comments(TEXT_WITH_LEADING_COMMENT);
        assert_eq!(text, "foo");
    }

    #[test]
    pub fn leading_comments_and_spaces_are_removed() {
        let text = remove_leading_html_comments(TEXT_WITH_LEADING_COMMENT_AND_SPACES);
        assert_eq!(text, "foo");
    }

    #[test]
    pub fn trailing_comments_are_not_removed() {
        let text = remove_leading_html_comments(TEXT_WITH_TRAILING_COMMENT);
        assert_eq!(text, TEXT_WITH_TRAILING_COMMENT);
    }

    #[test]
    pub fn infix_comments_are_not_removed() {
        let text = remove_leading_html_comments(TEXT_WITH_INFIX_COMMENT);
        assert_eq!(text, TEXT_WITH_INFIX_COMMENT);
    }
}
